import { vec3 } from "gl-matrix";
import { HasTexture } from "../render/types";
import { HasTransform } from "../world/types";
import { TiledImage } from "../common/tiled_image";


export type Heightmap = HasTexture & HasTransform & {
  canvas: HTMLCanvasElement,
  tile_cache: TiledImage | null,

};