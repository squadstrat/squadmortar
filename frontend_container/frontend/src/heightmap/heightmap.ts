import { mat4, vec3 } from "gl-matrix"
import {world2heightmap } from "../world/transformations";
import { $imagedataCache } from "./reducer";
import { Heightmap } from "./types"

export const getHeight = (heightmap: Heightmap, worldLoc: vec3) => {
  try {
    const heightmapPos = world2heightmap(heightmap, worldLoc).map(Math.floor);
    //console.log("heightmapPos", heightmapPos)
    //const dataIndex = (heightmapPos[1] * heightmap.size[0] + heightmapPos[0]) * 4
    const image_tile_cache = heightmap.tile_cache;
    //console.log("imagedataCache", image_tile_cache)
    /*if ($imagedataCache){
      const dataHigh = ($imagedataCache as any).data[dataIndex];
      const dataLow = ($imagedataCache as any).data[dataIndex + 1];
      const data = ((dataHigh << 8) + dataLow);
      const scale_z = mat4.getScaling(vec3.create(), heightmap.texture.transform)[2] / 100;
      const magic_number = 0.7808988764 // discrepancy between in-editor heights and exported heightmaps...
      return scale_z * data * magic_number;
    }*/
    if (image_tile_cache){
      return image_tile_cache.get(heightmapPos[0], heightmapPos[1]);
    }
    //imgdata?.data[dataIndex] || -1;
    console.error("no heightmap cache found")
    return 0
  } catch (error) {
    console.error("error in getHeight", error)
    return 0
  }
}